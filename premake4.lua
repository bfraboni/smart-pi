solution "gKit2light"
    
    configurations { "release", "debug" }
    
    -- switch gcc / clang
    premake.gcc.cc = "gcc-8"
    premake.gcc.cxx = "g++-8"
    -- premake.gcc.cc = "clang"
    -- premake.gcc.cxx = "clang++"

    platforms { "x64" }
    
    includedirs { ".", "sources" }
    
    configuration "debug"
        targetdir "bin/debug"
        defines { "DEBUG" }
        flags { "Symbols" }
        buildoptions { "-g"}
        linkoptions { "-g"}

    configuration "release"
        targetdir "bin/release"
        defines { "RELEASE" }
        flags { "OptimizeSpeed" }

    configuration "linux"
        buildoptions { "-mtune=native -march=native -std=c++11 -W -Wall -Wextra -Wsign-compare -Wno-unused-parameter -Wno-unused-function -Wno-unused-variable", "-pipe" }
        buildoptions { "-flto"}
        linkoptions { "-flto"}
        buildoptions { "-fopenmp" }
        linkoptions { "-fopenmp" }

project("smart-video")
    language "C++"
    kind "ConsoleApp"
    targetdir "bin"
    files { "sources/main.cpp" }

    configuration "linux"
        -- openvc 3.4.3
        opencv=  "opencv-install" 
        includedirs { opencv .. "/include" }
        libdirs { opencv .. "/lib" }
        linkoptions { "-Wl,-rpath," .. opencv .. "/lib" }
        links { "opencv_videoio", "opencv_imgproc", "opencv_highgui", "opencv_core" }