#include <iostream>

#include "opencv2/opencv.hpp"

int main ( int argc, char *argv[] )
{
    // open the defult camera device, corresponding to /dev/video0
    cv::VideoCapture capture(0);

    if( capture.isOpened() )
    {
        std::cout << "Video device opened ..." << std::endl;
        
        // camera setup
        int w_ratio = 2;
        int h_ratio = 2;
        capture.set(cv::CAP_PROP_FRAME_WIDTH, 1920 / w_ratio);
        capture.set(cv::CAP_PROP_FRAME_WIDTH, 1080 / h_ratio);

        // frame container
        cv::Mat frame;

        // loop
        while( cv::waitKey(20) != 'q' )
        {
            // grab current frame
            capture >> frame;
            
            // show the frame 
            cv::imshow("test", frame);
        }
    }   
    else
    {
        std::cerr << "Can not open video device ..." << std::endl;
        return 1;
    }

    capture.release();

    return 0;
}